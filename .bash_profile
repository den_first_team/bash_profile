alias vsc='/Applications/VisualStudioCode.app/Contents/Resources/app/bin/code'
alias vsc_sudo='sudo /Applications/VisualStudioCode.app/Contents/Resources/app/bin/code'
alias alias_show='vsc /Users/programmer_5/.bash_profile'
alias hosts_show='vsc_sudo /etc/hosts'
alias sites_show='open /Applications/MAMP/htdocs'
alias vhosts_show='vsc_sudo /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf'
alias bash_update='source ~/bash_user/.bash_profile'

alias aliasu='alias_show'
alias hosts='hosts_show'
alias vhosts='vhosts_show'
alias sites='sites_show'

install_yii () {
  composer create-project yiisoft/yii2-app-basic $1
}

hosts_add () {
   sudo sh -c "echo '127.0.0.1	$1' >> /etc/hosts"
#    sudo echo "127.0.0.1	$1" >> /etc/hosts
}

vhosts_add () {
    sudo sh -c "echo '<VirtualHost *:80>\nServerAdmin oshchyp.denys@gmail.com\nDocumentRoot /Applications/MAMP/htdocs/$1/web\nServerName $1\nServerAlias *.$1\n</VirtualHost>' >> /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf"
}

site_dir_add (){
    mkdir /Applications/MAMP/htdocs/$1
}

site_add (){
    hosts_add $1
    vhosts_add $1
    site_dir_add $1
}

site_yii_add (){
    site_add $1
    install_yii /Applications/MAMP/htdocs/$1
    echo 'RewriteEngine on\nRewriteCond %{REQUEST_FILENAME} !-f\nRewriteCond %{REQUEST_FILENAME} !-d\nRewriteRule ^(.*)$ index.php [L]' >> /Applications/MAMP/htdocs/$1/web/.htaccess
    vsc /Applications/MAMP/htdocs/$1
    sudo service apache2 restart
}

